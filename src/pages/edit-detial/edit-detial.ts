import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EditDetialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edit-detial',
  templateUrl: 'edit-detial.html',
})
export class EditDetialPage {

  value:any

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.value = navParams.get('item');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditDetialPage');
  }

}
