import { EditDetialPage } from './../edit-detial/edit-detial';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-edit-view',
  templateUrl: 'edit-view.html',
})
export class EditViewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  swipeAction(e,index) {
    if(e.direction == 2)
    {
      this.navCtrl.push(EditDetialPage, {
        item:index})
      console.log("go to edit page");
    }
    else
    {
      console.log("e.direction :: ",e.direction);
    }
    // let alert = this.alertCtrl.create({
    //   title: 'New Friend!',
    //   subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
    //   buttons: ['OK']
    // });
    // alert.present();
  }

  clickAction(e,index){
    this.navCtrl.push(EditDetialPage, {
      item:index})
    console.log("go to edit page");
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad EditViewPage');
  }

}
