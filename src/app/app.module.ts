import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CreatePage } from '../pages/create/create';
import { EditViewPage } from '../pages/edit-view/edit-view';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { EditDetialPage } from '../pages/edit-detial/edit-detial';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CreatePage,
    EditViewPage,
    LoginPage,
    DashboardPage,
    EditDetialPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CreatePage,
    EditViewPage,
    LoginPage,
    DashboardPage,
    EditDetialPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
